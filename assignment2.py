from flask import Flask, render_template, request, redirect, url_for, jsonify, json, Response
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': 1}, \
		 {'title': 'Algorithm Design', 'id':2}, \
		 {'title': 'Python', 'id':3}]

@app.route('/book/JSON')
def bookJSON():
    r = json.dumps(books)
    return Response(r)
    
@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books=books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        new_book = request.form['name']
        Dict = dict({'title': new_book , 'id':len(books)+1})
        books.append(Dict)
        return render_template('newBook.html')
    else:
        return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method =='POST':
        search_key = request.form['name']
        selected = books[book_id-1]
        selected['title'] = search_key
        books[book_id-1] = selected
        return render_template('showBook.html', books = books)
    else:
        pass
    return render_template('editBook.html', book_id=book_id)
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    name = books[book_id-1].get('title')
    if request.method == 'POST':
        print(name)
        del books[book_id-1]
        if len(books) >  0:
            for i in range(book_id-1, len(books)):
                x = books[i]
                x['id'] -= 1
                books[i] = x
        return render_template('showBook.html', books = books)

    return render_template('deleteBook.html', book_id=book_id, book_name = name)
if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)